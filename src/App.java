import com.devcamp.jbr2s10.Author;
import com.devcamp.jbr2s10.Book;

public class App {
    public static void main(String[] args) throws Exception {
        
        Author author1 = new Author("Minh Duc", "MinhDuc@gmail.com", 'M');
        Author author2 = new Author("Kim Huong", "Kimhuong@gmail.com", 'F');

        System.out.println("Author1: " + author1);
        System.out.println("Author2: " + author2);

        ///
        Book book1 = new Book("Chiec la cuoi cung", author1, 25000.0);
        Book book2 = new Book("Hat giong tam hon", author2, 27000.0, 6);

        System.out.println("Book1: " + book1);
        System.out.println("Book2: " + book2);
    }

}
